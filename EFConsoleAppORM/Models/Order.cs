﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFConsoleAppORM.Models
{
    public class Order
    {
        [Key]
        public int IdOrder { get; set; }
        public double Price { get; set; }
    }
}
